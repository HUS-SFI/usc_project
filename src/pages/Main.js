import React from "react"
import SearchBar from "../components/SearchBar"
import VideosList from "../components/VideosList"
import Video from "../components/Video"
import youtube, { getDefaultParams } from "../api/videoApi"
import styles from "./main.module.css"
// import Video from "../components/Video"
// const Video = React.lazy(()=>import("../components/Video"))
import { motion } from "framer-motion"
import ScaleLoader from "react-spinners/ScaleLoader"
class Main extends React.Component {
    state = {
        isSearched: false,
        videos: [],
        sellectedVideo: null,
        theTerm: "",
        done: false,
    }

    onItemClick = (item) => {
        this.setState({ sellectedVideo: item })
    }

    getUserInput = async (term) => {
        this.setState({ isSearched: true, done: false })
        const response = await youtube
            .get("/search", {
                params: {
                    ...getDefaultParams(),
                    q: term,
                },
            })
            .catch((e) => {
                console.log("error > ", e)
            })

        this.setState({
            videos: response.data.items,
            sellectedVideo: null,
            theTerm: term,
            done: true,
        })
    }

    // resultLoger = () => {
    //     if (this.state.videos.length > 0) {
    //         return <p>{this.state.videos.length} videos founded!</p>
    //     } else {
    //         return
    //     }
    // }

    waiteHandler = () => {
        if (this.state.isSearched && !this.state.done) {
            return (
                <div className="waite">
                    <ScaleLoader color={"orange"} />
                    <p>please waite</p>
                </div>
            )
        }
    }

    pageTransition = {
        in: {
            opacity: 1,
            x: 0,
        },
        out: {
            opacity: 0,
            x: 30,
        },
    }
    SearchBarTransition = {
        in: {
            opacity: 1,
            y: 0,
        },
        out: {
            opacity: 0,
            y: -50,
        },
    }
    render() {
        return (
            <motion.div
                initial="out"
                animate="in"
                exit="out"
                variants={this.pageTransition}
                className="App"
                style={{
                    color: "white",
                    maxWidth: "1300px",
                    margin: "auto",
                    backgroundColor: "#2A2E45",
                    // border: "1px solid #ccc",
                    // paddingTop: "1rem",
                    paddingTop: "1rem",
                    display: "flex",
                    flexDirection: "column",
                }}
            >
                {this.state.isSearched ? (
                    <>
                        <motion.div
                            variants={this.SearchBarTransition}
                            initial="out"
                            animate="in"
                            exit="out"
                            transition={{ duration: 0.5 }}
                            style={{
                                display: "flex",
                                justifyContent: "center",
                                flexDirection: "column",
                                textAlign: "center",
                            }}
                        >
                            <p
                                style={{
                                    color: "orange",
                                    margin: "1rem",
                                    fontSize: "1.2rem",
                                    fontWeight: "bold",
                                }}
                            >
                                Video Search
                            </p>
                            <SearchBar
                                getUserInput={this.getUserInput}
                                value={this.state.theTerm}
                            />
                            <p style={{ marginTop: "1rem" }}>
                                {this.state.theTerm}
                            </p>
                            {this.waiteHandler()}
                        </motion.div>
                    </>
                ) : (
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            textAlign: "center",
                            height: "70vh",
                            alignItems: "center",
                            flexDirection: "column",
                        }}
                    >
                        <h1
                            style={{
                                color: "orange",
                                margin: "1rem",
                                fontSize: "3rem",
                            }}
                        >
                            Video Search
                        </h1>
                        <p style={{ marginBottom: "0.8rem" }}>
                            please type something and then press Enter
                        </p>
                        <SearchBar getUserInput={this.getUserInput} />
                    </div>
                )}

                <div className={styles.main}>
                    <div style={{ flex: "5", marginTop: "3rem" }}>
                        {this.state.sellectedVideo ? (
                            <Video
                                video={this.state.sellectedVideo}
                                className={styles.video}
                            />
                        ) : (
                            ""
                        )}
                    </div>

                    <div className="sidebar" style={{ alignSelf: "flex-end" }}>
                        <VideosList
                            videos={this.state.videos}
                            onItemClick={this.onItemClick}
                        />
                    </div>
                </div>
            </motion.div>
        )
    }
}

export default Main
