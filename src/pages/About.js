import React from "react"
import { motion } from "framer-motion"
const About = (props) => {
    const pageTransition = {
        in: {
            opacity: 1,
            x: 0,
        },
        out: {
            opacity: 0,
            x: 30,
        },
        
    }
    return (
        <>
            <motion.div
                animate="in"
                initial="out"
                exit="in"
                variants={pageTransition}
                
                style={{
                    maxWidth: "1200px",
                    margin: "1rem auto",
                    padding: "0.4rem",
                }}
            >
                <p style={{ color: "white" }}>
                    Undergraduate project of the University of Science and
                    Culture | Hossein Seifi | search and play videos | using
                    React js , React-Router-dom , youtube API
                </p>
                <a
                    style={{ color: "orange" }}
                    href="https://gitlab.com/HUS-SFI/usc_project"
                >
                    project's GitLab repository
                </a>
            </motion.div>
        </>
    )
}

export default About
