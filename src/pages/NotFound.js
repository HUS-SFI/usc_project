import React from "react"

const NotFound = () => {
    return (
        <div
            style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                color: "white",
                width: "100vw",
                height: "90vh",
            }}
        >
            <p
                style={{
                    display: "flex",
                    fontSize: "2rem",
                    color: "white",
                }}
            >
                <span style={{ color: "orange" }}> 404 </span> | not found
            </p>
        </div>
    )
}

export default NotFound
