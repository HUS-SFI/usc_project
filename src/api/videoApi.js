import axios from "axios"
// const KEY = "AIzaSyAMn9o5fcgnJjAve9BpARAvVa9jxKdGffY"
const KEY = "AIzaSyB9p9l2kNu1PiRo3RX4YQ8Go_d_bWiSqfk"

export default axios.create({
    baseURL: "https://www.googleapis.com/youtube/v3",
})

export const getDefaultParams = () => {
    return {
        part: "snippet",
        maxResults: 6,
        key: KEY,
    }
}
