import React from "react"
import { NavLink, withRouter } from "react-router-dom"

const Navigator = (props) => {
    const nav = [
        { exact: true, to: "/", name: "Home" },
        {
            to: "/about",
            name: "About",
        },
    ]

    return (
        <div className="navbar">
            <ul
                style={{
                    display: "flex",
                    listStyle: "none",
                    // backgroundColor: "white",
                    justifyContent: "flex-end",
                }}
            >
                {nav.map((el) => {
                    return (
                        <li style={{ margin: "0.4rem 1rem" }} key={el.name}>
                            <NavLink
                                style={{
                                    textDecoration: "none",
                                    color: "white",
                                }}
                                exact={el.exact || false}
                                activeStyle={{
                                    color: "orange",
                                }}
                                to={el.to}
                            >
                                {el.name}
                            </NavLink>
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}

export default withRouter(Navigator)
