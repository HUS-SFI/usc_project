import React from "react"
import Video from "./VideoItem"

const VideosList = (props) => {
    const getClickedItem = (item) => {
        props.onItemClick(item)
    }
  
    return (
        <div
        
        
        >
            {props.videos.map((item) => {
                return (
                    <Video
                        video={item}
                        key={item.id.videoId}
                        getClickedItem={getClickedItem}
                    />
                )
            })}
        </div>
    )
}

export default VideosList
