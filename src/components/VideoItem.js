import React from "react"
import styles from "./videoItem.module.css"
import { motion } from "framer-motion"
const Video = (props) => {
    const onUserClick = () => {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth",
        })
        props.getClickedItem(props.video)
    }
    const pageTransition = {
        in: {
            opacity: 1,
            x: 0,
        },
        out: {
            opacity: 0,
            x: 30,
        },
    }
    return (
        <motion.div
            variants={pageTransition}
            animate="in"
            initial="out"
            exit="in"
            transition={{ duration: 0.5 }}
            className={styles.item}
            onClick={onUserClick}
        >
            <img
                src={props.video.snippet.thumbnails.medium.url}
                alt=""
                className={styles.image}
            />
            <div style={{ color: "white" }}>{props.video.snippet.title}</div>
        </motion.div>
    )
}

export default Video
