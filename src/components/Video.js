import React, { Fragment } from "react"
import styles from "./video.module.css"
import { motion } from "framer-motion"
class Video extends React.Component {
    Transition = {
        in: {
            opacity: 1,
            y: 0,
        },
        out: {
            opacity: 0,
            y: -50,
        },
    }
    render() {
        let title = ""
        let discr = ""
        let address = `https://www.youtube.com/embed/${this.props.video.id.videoId}`
        if (this.props.video) {
            title = this.props.video.snippet.title
            discr = this.props.video.snippet.description
        } else {
            title = "loading ... "
            discr = ""
        }
        return (
            <Fragment>
                <motion.div
                    variants={this.Transition}
                    animate="in"
                    initial="out"
                    exit="in"
                    transition={{ duration: 0.5 }}
                    className={styles.video}
                >
                    <iframe
                        src={address}
                        title="video player"
                        className={styles.videoinit}
                    ></iframe>

                    <strong>{title}</strong>
                    <p>{discr}</p>
                </motion.div>
            </Fragment>
        )
    }
}

export default Video
