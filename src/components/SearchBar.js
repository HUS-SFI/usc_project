import React from "react"
import { motion } from "framer-motion"
class SearchBar extends React.Component {
    state = { term: "" }
    pageTransition = {
        in: {
            opacity: 1,
            y: 0,
        },
        out: {
            opacity: 0,
            y: 30,
        },
    }
    onFormSubmit = (event) => {
        event.preventDefault()
        this.props.getUserInput(this.state.term)
    }

    onUserInputChange = (event) => {
        this.setState({ term: event.target.value })
    }
    render() {
        return (
            <motion.div
              
                className="search-bar"
                style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
            >
                <form onSubmit={this.onFormSubmit}>
                    <input
                        style={{
                            display: "flex",
                            alignSelf: "center",
                            justifySelf: "center",
                            width: "20rem",
                            height: "2rem",
                            borderRadius: "5px",
                            fontSize: "1.2rem",
                            textAlign: "center",
                            // backgroundColor: "#585658",
                        }}
                        type="text"
                        placeholder="Search"
                        value={this.state.term}
                        // value={this.state.term?this.state.term:this.props.value}
                        onChange={this.onUserInputChange}
                    />
                </form>
            </motion.div>
        )
    }
}

export default SearchBar
