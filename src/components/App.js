import React from "react"
import { Route, Switch, BrowserRouter } from "react-router-dom"
import Routes from "../routes"
import Lauout from "./Layout"
import { AnimatePresence } from "framer-motion"
const App = (props) => {
    return (
        <div className="App">
            <BrowserRouter>
                <Lauout>
                    <AnimatePresence exitBeforeEnter>
                        <Switch>
                            {Routes.map((route) => {
                                return <Route {...route} />
                            })}
                        </Switch>
                    </AnimatePresence>
                </Lauout>
            </BrowserRouter>
        </div>
    )
}

export default App
