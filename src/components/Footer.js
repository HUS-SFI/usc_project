import React from "react"

const Footer = () => {
    return (
        <div
         
       
        >
            <div
                className="footer"
                style={{
                    padding: "0.3rem",
                    // marginTop: "1rem",
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <div
                    style={{
                        color: "white",
                        fontSize: "1.3rem",
                        display: "inline",
                        padding: "1rem",
                    }}
                >
                    Video Search
                </div>
                <a
                    style={{ textDecoration: "none" }}
                    href="https://gitlab.com/HUS-SFI/usc_project"
                >
                    <i
                        style={{
                            color: "orange",
                            fontSize: "1.8rem",
                            // margin: "0.5rem",
                            // display: "block",
                        }}
                        className="bx bxl-gitlab"
                    ></i>
                </a>{" "}
            </div>
        </div>
    )
}

export default Footer
