import Main from "./pages/Main"
import About from "./pages/About"
import NotFound from "./pages/NotFound"
const routes = [
    {
        path: "/",
        exact: true,
        component: Main,
    },
    {
        path: "/about",
        component: About,
    },
    {
        component: NotFound,
    },
]

export default routes
